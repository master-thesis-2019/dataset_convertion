scenarios = [
    'bahnhof',
    'jelmoli',
    'linthescher',
    'loewenplatz',
    'sunny_day'
]

replace_str = '&&&'
in_annotations_dir_template = 'D:\\university\\master\\datasets_original\\ethz\\{}\\original_annotations.txt'.format(replace_str)
out_annotation_file = 'D:\\university\\master\\datasets_convertion\\ethz\\annotations.txt'
out_frames_dir_template = 'ethz/{}'.format(replace_str)


def parse_bbox(column):   
    return column.replace('(', '').replace('),', ',0 ').replace(');', ',0').replace(').', ',0')

def parse_bboxes(columns):

    bboxes = []

    for i, column in enumerate(columns):

        if i == 0:
            continue

        bboxes.append(parse_bbox(column))
    
    return bboxes


def parse_frame_filename(columns):
    return columns[0].replace('"', '').replace(':', '').replace('left/', '') 


def add_line_to_out_annotation_file(out_file, out_frames_dir, frame_filename, bboxes):

    if bboxes.count == 0:
        return

    out_file.write('{}/{} '.format(out_frames_dir, frame_filename))

    for bbox in bboxes:
        out_file.write(bbox)

    out_file.write('\n')


def main():
    
    with open(out_annotation_file, 'w+') as out_file:
        
        for scenario in scenarios:

            in_annotations_dir = in_annotations_dir_template.replace(replace_str, scenario)
            out_frames_dir = out_frames_dir_template.replace(replace_str, scenario)

            with open(in_annotations_dir, 'r+') as in_file:
                
                print('in_annotations_dir: {}'.format(in_annotations_dir))
                print('out_frames_dir: {}'.format(out_frames_dir))

                for line in in_file:

                    columns = line.split()
                    frame_filename = parse_frame_filename(columns)
                    bboxes = parse_bboxes(columns)

                    add_line_to_out_annotation_file(out_file, out_frames_dir, frame_filename, bboxes)

main()