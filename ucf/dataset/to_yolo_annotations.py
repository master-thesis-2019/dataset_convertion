import os
import xml.etree.ElementTree as ET

scenarios = [
    'actions1',
    'actions2',
    'actions3',
    'aerial_tape1_part1',
    'aerial_tape1_part2',
    'aerial_tape2_part1',
    'aerial_tape3_part1',
]

replace_str = '$$$'

in_annotations_dir_template = 'D:/university/master/datasets_convertion/ucf/dataset/{}/original_annotations'.format(replace_str)
out_annotation_file = 'D:/university/master/datasets_convertion/ucf/dataset/annotations.txt'

def main():

    with open(out_annotation_file, 'w+') as out_file:

        for scenario in scenarios:
            in_annotations_dir = in_annotations_dir_template.replace(replace_str, scenario)
            in_annotations_dir_encoded = os.fsencode(in_annotations_dir)

            for file in os.listdir(in_annotations_dir_encoded):

                root = ET.parse('{}/{}'.format(in_annotations_dir, os.fsdecode(file))).getroot()
                
                scenario = root.find('folder').text
                filename= root.find('filename').text
                line = line = 'ucf/{}/{}/{}'.format(scenario, scenario, filename)
                bboxes = root.findall('object')

                for bbox in bboxes:

                    label = bbox.find('name').text
                    xmin = bbox.find('bndbox/xmin').text
                    ymin = bbox.find('bndbox/ymin').text
                    xmax = bbox.find('bndbox/xmax').text
                    ymax = bbox.find('bndbox/ymax').text

                    line = '{} {},{},{},{},{}'.format(line, xmin, ymin, xmax, ymax, label)
                    
                out_file.write('{}\n'.format(line))

main()