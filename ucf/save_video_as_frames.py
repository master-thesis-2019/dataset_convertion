import cv2

scenarios = [
    'aerial_tape1_part1',
    'aerial_tape1_part2',
    'aerial_tape2_part1',
    'aerial_tape3_part1'
]

skip_frames = 60
replace_str = '$$$'
input_video_filename_template = "D:\\university\\master\\datasets_original\\UCF\\$$$.mpg"
output_dir_template = 'D:\\university\\master\\datasets_convertion\\ucf\\dataset\\$$$'

def main():
    for scenario in scenarios:
        input_video_filename = input_video_filename_template.replace(replace_str, scenario)
        output_dir = output_dir_template.replace(replace_str, scenario)

        vidcap = cv2.VideoCapture(input_video_filename)
        frame_id = -1
        success = True
        print('In progress {} ...'.format(scenario))
        print('input_input_video_filename {}'.format(input_video_filename))
        print('output_dir {}'.format(output_dir))

        while True:
            success, image = vidcap.read()
            if not success:
                break
            
            frame_id += 1
            if frame_id % skip_frames != 0:
                continue 

            cv2.imwrite('{0}\\{1}.jpg'.format(output_dir, frame_id), image)

        print('Done {}'.format(scenario))


    vidcap.release()
    print('Done all')

main()