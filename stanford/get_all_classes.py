#!/usr/bin/python

filename = 'D:\\university\\master\\datasets_original\\Stanford\\annotations\\bookstore\\video0\\annotations.txt'
label_index = 9
labels = {}


def main():
    with open(filename) as file:
        for line in file:
            columns = line.split()            
            label = columns[label_index]

            if label not in labels:
                labels[label] = label
    print(labels)

main()