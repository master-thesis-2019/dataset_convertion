scenarios = [
    'bookstore',
    'coupa',
    'deathCircle',
    'gates',
    'hyang',
    'little',
    'nexus',
    'quad'
]

allowed_labels = ['Biker', 'Pedestrian', 'Skater']

stanford_annotations_dir = 'D:/university/master/datasets_original/Stanford/annotations'
out_annotations = 'D:/university/master/src_datasets_convertion/stanford/annotations'

replace_str = '&&&'
out_frames_dir_template = 'stanford/{}'.format(replace_str)
stanford_annotations_filename_template = '{}/{}/video0/annotations.txt'.format(stanford_annotations_dir, replace_str)
out_annotations_filename_template = '{}/annotations_{}.txt'.format(out_annotations, replace_str)

xmin_index = 1
ymin_index = 2
xmax_index = 3
ymax_index = 4
frame_index = 5
is_lost_index = 6
is_occluded_index = 7
label_index = 9

yolo_person_class_id = 0

frames_skip_count = 20

class Detection:
    def __init__(self, frame_id, x_min, y_min, x_max, y_max, label):
        self.frame_id = frame_id
        self.x_min = x_min
        self.y_min = y_min
        self.x_max = x_max
        self.y_max = y_max
        self.label = label

def parse_detection_from_row(columns):
    return Detection(columns[frame_index], columns[xmin_index], columns[ymin_index], columns[xmax_index], columns[ymax_index], columns[label_index])

def parse_frames_with_detections(stanford_annotations_filename, skip_count=1):
    frames = { }

    with open(stanford_annotations_filename) as file:

        for line in file:

            columns = line.split()
            label = columns[label_index].replace('"', '')
            is_lost = columns[is_lost_index] == '1'
            is_occluded = columns[is_occluded_index] == '1'

            if label in allowed_labels and not is_lost and not is_occluded:
                detection = parse_detection_from_row(columns)

                if detection.frame_id in frames:
                    frames[detection.frame_id].append(detection)
                else:
                    frames[detection.frame_id] = [ detection ]

    return frames

def get_detection_str(detection):
    return ' {},{},{},{},{}'.format(detection.x_min, detection.y_min, detection.x_max, detection.y_max, yolo_person_class_id)

def save_detections_in_yolo_format(frames_with_detections, out_frames_dir, out_annotations_filename):

    with open(out_annotations_filename, 'w+') as file:

        for frame_id, detections in frames_with_detections.items():

            if (int(frame_id) % 20 != 0):
                continue

            file.write('{}/{}.jpg'.format(out_frames_dir, frame_id))

            for detection in detections:
                file.write(get_detection_str(detection))

            file.write('\n')

def get_scenario_filepaths(scenario):
    out_frames_dir = out_frames_dir_template.replace(replace_str, scenario)
    stanford_annotations_filename = stanford_annotations_filename_template.replace(replace_str, scenario)
    out_annotations_filename = out_annotations_filename_template.replace(replace_str, scenario)

    return out_frames_dir, stanford_annotations_filename, out_annotations_filename

def main():

    for scenario in scenarios:        
        out_frames_dir, stanford_annotations_filename, out_annotations_filename = get_scenario_filepaths(scenario)

        frames = parse_frames_with_detections(stanford_annotations_filename)
        save_detections_in_yolo_format(frames, out_frames_dir, out_annotations_filename)
                
main()