scenarios = [
    'bookstore',
    'coupa',
    'deathCircle',
    'gates',
    'hyang',
    'little',
    'nexus',
    'quad'
]

replace_str = '&&&'

src_annotations = 'D:/university/master/src_datasets_convertion/stanford/annotations'
src_annotations_filename_template = '{}/annotations_{}.txt'.format(src_annotations, replace_str)
dest_filepath = 'D:/university/master/done/annotations/stanford.txt'

def copy_annotation_file_content(src_filepath, dest_file):
    with open(src_filepath,'r+') as src_file:
        for line in src_file:
            dest_file.write(line)

def main():    
    with open(dest_filepath, 'w+') as dest_file:
        for scenario in scenarios:
            src_filepath = src_annotations_filename_template.replace(replace_str, scenario)
            print(src_filepath)
            copy_annotation_file_content(src_filepath, dest_file)

main()