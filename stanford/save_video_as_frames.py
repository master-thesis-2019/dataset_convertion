import cv2

scenarios = [
    'bookstore',
    'coupa',
    'deathCircle',
    'gates',
    'hyang',
    'little',
    'nexus',
    'quad'
]

video_filename_template = "D:\\university\\master\\datasets_original\\Stanford\\video\\yyy\\video0\\video.mov"
output_dir_template = 'D:\\university\\master\\src_datasets_convertion\\stanford\\frames\\yyy'
skip_frames_count = 20

def main():
    for scenario in scenarios:
        video_filename = video_filename_template.replace('yyy', scenario)
        output_dir = output_dir_template.replace('yyy', scenario)

        vidcap = cv2.VideoCapture(video_filename)
        frame_id = 0
        success = True
        print('In progress {} ...'.format(scenario))
        print('video_filename {}'.format(video_filename))
        print('output_dir {}'.format(output_dir))

        while True:
            success, image = vidcap.read()
            if not success:
                break

            if (frame_id % skip_frames_count == 0):
                cv2.imwrite('{0}\\{1}.jpg'.format(output_dir, frame_id), image)
                
            frame_id += 1

        print('Done {}'.format(scenario))


    vidcap.release()
    print('Done all')

main()