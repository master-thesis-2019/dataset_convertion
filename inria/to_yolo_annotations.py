import os
import xml.etree.ElementTree as ET

in_annotations_dir = 'D:/university/master/label_img_annotations/inria'
out_annotation_file = 'D:/university/master/done/annotations/inria.txt'

def main():

    with open(out_annotation_file, 'w+') as out_file:
        
        in_annotations_dir_encoded = os.fsencode(in_annotations_dir)

        for file in os.listdir(in_annotations_dir_encoded):

            root = ET.parse('{}/{}'.format(in_annotations_dir, os.fsdecode(file))).getroot()
            
            filename= root.find('filename').text
            line = line = 'inria/{}'.format(filename)
            bboxes = root.findall('object')

            for bbox in bboxes:

                label = bbox.find('name').text
                xmin = bbox.find('bndbox/xmin').text
                ymin = bbox.find('bndbox/ymin').text
                xmax = bbox.find('bndbox/xmax').text
                ymax = bbox.find('bndbox/ymax').text

                line = '{} {},{},{},{},{}'.format(line, xmin, ymin, xmax, ymax, label)
                
            out_file.write('{}\n'.format(line))

main()